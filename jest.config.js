module.exports = {
    "browser": true,
    "bail": true,
    "verbose": true,
    "notify": true,
    "unmockedModulePathPatterns": [
        "/node_modules/react"
    ],
    "testPathIgnorePatterns": [
        "/node_modules/",
        "/config/webpack/test.js",
        "./App.test.js"
    ],
    "testMatch": [
        '<rootDir>/(tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/__test__/*.(js|jsx|ts|tsx))'
    ],
    "transformIgnorePatterns": [
        "/node_modules/",
        "/node_modules/@atlaskit"
    ],
    transform: {
        "^.+\\.(ts|tsx|js|jsx)$": "<rootDir>/node_modules/babel-jest",
    },
    "setupFiles": ["./setupTests.js"]
};