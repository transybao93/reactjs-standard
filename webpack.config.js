const HtmlWebPackPlugin = require("html-webpack-plugin");
const NodemonPlugin = require( 'nodemon-webpack-plugin' );
const path = require('path');
const Dotenv = require('dotenv-webpack');
const autoprefixer = require("autoprefixer");
const webpack = require("webpack");
const StyleLintPlugin = require('stylelint-webpack-plugin');

const htmlWebpackPlugin = new HtmlWebPackPlugin({
    template: "./public/index.html",
    filename: "./index.html",
    favicon: './public/favicon.ico'
});

module.exports = {
    mode: 'production',
    entry: './src/index.js',
    // entry: { main: './src/index.js' },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'index.bundle.js',
        // globalObject: 'this'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif|ico)$/,
                exclude: /node_modules/,
                use: ['image-webpack-loader', 'file-loader?name=[name].[ext]'], // ?name=[name].[ext] is only necessary to preserve the original file name
                enforce: 'pre'
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: "style-loader",
                        options: {
                            sourceMap: true,
                        }
                    },
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true,
                        }
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            includePaths: [path.resolve(__dirname, "src")],
                            outputStyle: 'compressed',
                            sourceMap: true,
                        }
                    }
                ],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['eslint-loader'],
            }
        ]
    },
    plugins: [
        htmlWebpackPlugin,
        // new NodemonPlugin({
        //     watch: path.resolve('./src'),
        //     verbose: true,
        // }),
        new Dotenv({
            path: './.env', // load this now instead of the ones in '.env'
            safe: false, // load '.env.example' to verify the '.env' variables are all set. Can also be a string to a different file.
            systemvars: true, // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
            silent: true, // hide any errors
            defaults: false // load '.env.defaults' as the default values if empty.
        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [
                    autoprefixer()
                ]
            }
        }),
        new StyleLintPlugin({
            failOnError: true,
            configFile: '.stylelintrc'
        }),
    ],
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        }
    },
    resolve: {
        extensions: [".js", ".jsx"]
    },
    devServer: {
        historyApiFallback: true,
    },
    performance: {
        maxEntrypointSize: 400000
    }
};