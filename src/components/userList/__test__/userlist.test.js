// import React from 'react';
// import ReactDOM from 'react-dom';
// import App from '../../../App';

// it('show homepage without crash', (done) => {
//     const div = document.createElement('div');
//     ReactDOM.render(<App />, div);
//     ReactDOM.unmountComponentAtNode(div);
//     done();
// });

import React from 'react';
import { shallow } from 'enzyme';
import {
	BrowserRouter as Router
} from 'react-router-dom';

function sum(a, b) {
    return a + b;
}
describe('test homepage', () => {
    it('should render without crash', () => {
        const wrapper = shallow(<div><b>important</b></div>);
        const route = shallow(<Router/>);
        expect(wrapper.text()).toEqual('important');
        expect(wrapper.find('b')).toHaveLength(1);
        expect(route.find(<p/>)).toHaveLength(0);
    });

    it('should tobe 2', () => {
        /*eslint-disable */
        expect(sum(1,1)).toBe(2);
        /*eslint-disable */
    });
    
    it('should equal to 2', () => {
        expect(2).toEqual(2);
    });
});

