import React, { Component } from 'react';
import maxresdefault from '../images/maxresdefault.jpg';
import { Link } from 'react-router-dom';

class HomePage extends Component {
    render() {
        return (
            <div>
                <img src={maxresdefault} alt="Logo" />
                <p>This is home page</p>
                <p>
                    <Link to="/user-list">user list</Link>
                </p>
            </div>
        );
    }
}

export default HomePage;
