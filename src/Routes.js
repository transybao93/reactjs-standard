import React, { Component } from 'react';
import {
	Route,
	Link,
	Switch
} from 'react-router-dom';
import HomePage from './components/home/pages/HomePage';
import ProductDetailPage from './components/detailProduct/pages/ProductDetailPage';
import ProductListPage from './components/listProduct/pages/ProductListPage';
import UserListPage from './components/userList/pages/UserListPage';

class Routes extends Component {
  render() {
    return (
        <div>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/product-detail">Product detail</Link>
                </li>
                <li>
                    <Link to="/user-list">User list</Link>
                </li>
                <li>
                    <Link to="/product-list">Product list</Link>
                </li>
            </ul>
            <Switch>
                <Route exact path="/" component={HomePage} />
                <Route path="/product-detail" component={ProductDetailPage} />
                <Route path="/product-list" component={ProductListPage} />
                <Route path="/user-list" component={UserListPage} />
            </Switch>
        </div>
    );
  }
}

export default Routes;
