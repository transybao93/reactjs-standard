FROM mhart/alpine-node:11 AS builder
WORKDIR /app
COPY . .
RUN npm rebuild node-sass && npm run build

# FROM mhart/alpine-node
# RUN yarn global add serve
# WORKDIR /app
# COPY --from=builder /app/dist .
# # CMD ["serve", "-p", "80", "-s", "."]
# CMD ["serve", "-s", "build", "-l", "80"]

FROM nginx:1.17.1-alpine
COPY --from=builder /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
